#include "ESP32_IoT_Google_JWT_ECCX08.h"

using namespace std;

String getJwt() {
  return ESP32_IoT_Core::ESP32_ECCX08::getJwt();
}

namespace ESP32_IoT_Core {

// Initialize WiFi and MQTT for this board
Client* netClient;
CloudIoTCoreDevice* device;
CloudIoTCoreMqtt* mqtt;
MQTTClient* mqttClient;
unsigned long iss = 0;
String jwt;

MQTTClient*
getMQTTClient()
{
  return mqttClient;
}
CloudIoTCoreMqtt*
getCoreMQTT()
{
  return mqtt;
}

void
initESP32IoTCore()
{
  // Initialize the ATEC608A
  ESP32_ECCX08::initECCX08();
  // Initialize the WiFi
  ESP32_WiFi::setupWifi();
  ESP32_MQTT::initMQTTGoogle();
}

namespace ESP32_ECCX08 {

void
initECCX08()
{
  // Init I2C Port
  Wire.begin();

  // Check the connection with ATEC608A
  if (!ECCX08.begin()) {
    Serial.println(
      "Device not found. Check wiring: "
      "https://gitlab.com/ever3001/"
      "m_sesi_s2_psesi#connection-between-esp32-evb-and-atecc608a-trust");
    while (1)
      ;
  } else {
    Serial.println("Successful. I2C connections are good.");
  }

  // Verify if the chip is locked
  if (!ECCX08.locked()) {
    Serial.println(
      "The ECCX08 on your board is not locked. Proceed to lock it");

    if (!ECCX08.writeConfiguration(ECCX08_DEFAULT_TLS_CONFIG)) {
      Serial.println("Writing ECCX08 configuration failed!");
      while (1)
        ;
    }

    if (!ECCX08.lock()) {
      Serial.println("Locking ECCX08 configuration failed!");
      while (1)
        ;
    }

    Serial.println("ECCX08 locked successfully");
    Serial.println();
  }
  // Create public key by using private key
  String publicKeyPem = ECCX08JWS.publicKey(ATEC608_DEFAULT_SLOT, false);
  // If public key is NULL or empty
  if (!publicKeyPem || publicKeyPem == "") {
    // Creates a new private key.
    publicKeyPem = ECCX08JWS.publicKey(ATEC608_DEFAULT_SLOT, true);
    // Test if the new private key was created and can create a public key
    if (!publicKeyPem || publicKeyPem == "") {
      Serial.println("Error generating public key!");
      while (1)
        ;
    }
  }
  cout << "My public key in slot " << ATEC608_DEFAULT_SLOT << " is : " << endl;
  Serial.println(publicKeyPem);
}

String
getJwt()
{
  // Get the time for now
  unsigned long now = time(nullptr);

  // calculate the JWT, based on:
  //   https://cloud.google.com/iot/docs/how-tos/credentials/jwts
  // JSON for the header. ALGORITHM & TOKEN TYPE
  JSONVar jwtHeader;
  // JSON for the payload. DATA
  JSONVar jwtPayload;

  // Asymmetric Key Cryptography algorithm Elliptic Curve Digital Signature
  // Algorithm using P-256 and SHA-256.
  jwtHeader["alg"] = "ES256";
  jwtHeader["typ"] = "JWT";

  jwtPayload["aud"] = SECRET_PROJECT_ID;
  jwtPayload["iat"] = now;
  jwtPayload["exp"] = now + (24L * 60L * 60L); // expires in 24 hours

  String JwtBuff =
    ECCX08JWS.sign(0, JSON.stringify(jwtHeader), JSON.stringify(jwtPayload));
  
  device->updateJWT(JwtBuff, GOOGLE_JWT_EXP_SECS);

#ifdef DEBUG
  cout << "Jwt = " << JwtBuff.c_str() << endl;
#endif // DEBUG

  return JwtBuff;
}
}

namespace ESP32_WiFi {

void
setupWifi()
{
  cout << "Starting wifi" << endl;
  connectWifi();
  // Init the time
  configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_PRIMARY, NTP_SECONDARY);
  cout << "Waiting on time sync..." << endl;
  while (time(nullptr) < 1510644967) {
    delay(10);
  }
  printLocalTime();
}

void
connectWifi()
{
  // Mode Station. Mode to connect to a wifi
  WiFi.mode(WIFI_STA);
  cout << "Attempting to connect network, SSID: " << SECRET_SSID << endl;
  cout << "Connecting wifi..." << flush;
  while (WiFi.status() != WL_CONNECTED) {
    cout << "." << flush;
    WiFi.begin(SECRET_SSID, SECRET_PASS);
    delay(1500);
  }
  cout << endl;
  cout << "You're connected to the network!" << endl;
}

void
printLocalTime()
{
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    cout << "Failed to obtain time" << endl;
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

String
getRSSIWiFi()
{
  return "Wifi: " + String(WiFi.RSSI()) + "db";
}

}

namespace ESP32_MQTT {

void
initMQTTGoogle()
{
  device = new CloudIoTCoreDevice(SECRET_PROJECT_ID,
                                  SECRET_CLOUD_REGION,
                                  SECRET_REGISTRY_ID,
                                  SECRET_DEVICE_ID,
                                  PRIVATE_KEY_STR);
  netClient = new WiFiClientSecure();
  mqttClient = new MQTTClient(512);
  mqttClient->setOptions(180, true, 1000); // keepAlive, cleanSession, timeout
  mqtt = new CloudIoTCoreMqtt(mqttClient, netClient, device);
  mqtt->setUseLts(true);
  mqtt->startMQTT();
}

void
connect()
{
  if (WiFi.status() != WL_CONNECTED) {
    ESP32_WiFi::connectWifi();
  }
  mqtt->mqttConnect();
}

bool
publishTelemetry(String data)
{
  return mqtt->publishTelemetry(data);
}

bool
publishTelemetry(const char* data, int length)
{
  return mqtt->publishTelemetry(data, length);
}

bool
publishTelemetry(String subfolder, String data)
{
  return mqtt->publishTelemetry(subfolder, data);
}

bool
publishTelemetry(String subfolder, const char* data, int length)
{
  return mqtt->publishTelemetry(subfolder, data, length);
}

}

namespace ESP32_GoogleIoTCore {

String
calculateClientId()
{
  String clientId;

  // Format:
  //
  //   projects/{project-id}/locations/{cloud-region}/registries/{registry-id}/devices/{device-id}
  //

  clientId += "projects/";
  clientId += SECRET_PROJECT_ID;
  clientId += "/locations/";
  clientId += SECRET_CLOUD_REGION;
  clientId += "/registries/";
  clientId += SECRET_REGISTRY_ID;
  clientId += "/devices/";
  clientId += SECRET_DEVICE_ID;

  return clientId;
}

}

}