#ifndef _ESP32_IOT_GOOGLE_JWT_ECCX08_H_
#define _ESP32_IOT_GOOGLE_JWT_ECCX08_H_

#include "ESP32_IoT_Google_JWT_ECCX08_Setup.h"

#include <Arduino.h>
#include <iostream>

#include <Arduino_JSON.h>
#include <Client.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>

#include <ArduinoECCX08.h>
#include <Wire.h>
#include <utility/ECCX08DefaultTLSConfig.h>
#include <utility/ECCX08JWS.h>

#include <MQTT.h>

#include <CloudIoTCore.h>
#include <CloudIoTCoreMqtt.h>

namespace ESP32_IoT_Core {

MQTTClient* getMQTTClient();
CloudIoTCoreMqtt* getCoreMQTT();

void
initESP32IoTCore();

namespace ESP32_ECCX08 {

/* Initialize the crypto processor*/
void
initECCX08();
String
getJwt();

}

namespace ESP32_WiFi {

void
setupWifi();
void
connectWifi();
void
printLocalTime();
String
getRSSIWiFi();

}

namespace ESP32_MQTT {

void
initMQTTGoogle();
void
connect();
bool
publishTelemetry(String data);
bool
publishTelemetry(const char* data, int length);
bool
publishTelemetry(String subfolder, String data);
bool
publishTelemetry(String subfolder, const char* data, int length);

}

namespace ESP32_GoogleIoTCore {

String
calculateClientId();
}

}

#endif //! _ESP32_IOT_GOOGLE_JWT_ECCX08_H_