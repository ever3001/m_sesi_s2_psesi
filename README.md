# Evaluate the use of a low-cost cryptoprocessor in IoT project

Project webpage (in french): https://ever3001.gitlab.io/m_sesi_s2_psesi/

The obejctive of this project is to connect the board [ESP32-EVB](https://www.olimex.com/Products/IoT/ESP32/ESP32-EVB/open-source-hardware) using the wifi module, the protocol [MQTT](https://mqtt.org/), the crypto processor [ATEC608 Trust](https://www.microchip.com/developmenttools/ProductDetails/DT100104) and the authentication method [JSON Web Tokens](https://jwt.io/) to communicate with [Google Cloud IoT Core](https://cloud.google.com/solutions/iot).

The code sends a message to the ***/devices/{deviceId}/state*** topic every 5 seconds with the WiFi RSS value and listens for messages on both ***/devices/{deviceId}/config*** topic and ***/devices/{deviceId}/commands/#*** topics. It is possible to send the following commands:

| Command    |       Description       |
|:----------:|:-----------------------:|
|  **R1ON**  | Turn on the relay 1     |
|  **R1OFF** | Turn off the relay 1    |
|  **R2ON**  | Turn on the relay 2     |
|  **R2OFF** | Turn off the relay 2    |

---

[[_TOC_]]

## Connection between ESP32-EVB and ATECC608A Trust

Before building the project, it is necessary to realize the follow circuit.

![Connection between ESP32-EVB and ATECC608A Trust](./assets/img/I2C_ESP_ATEC.jpg "Connection between ESP32-EVB and ATECC608A Trust")

For more information, please see the [ESP32-EVB latest schematic in PDF format](https://github.com/OLIMEX/ESP32-EVB/raw/master/HARDWARE/REV-F/ESP32-EVB_Rev_F.pdf) or the [ESP32-EVB GitHub repository for the hardware design](https://github.com/OLIMEX/ESP32-EVB)

## Compiling and uploading the project

- This project was made with [PlatformIO](https://platformio.org/). To proceed to compile, install [PlatformIO IDE for VSCode](https://platformio.org/platformio-ide)
- Clone the repository

```bash
git clone https://gitlab.com/ever3001/m_sesi_s2_psesi.git
```

- Open the project with PlatformIO IDE for VSCode. PIO Home > Open Project.
- Fill the information of the WiFi network and Google Cloud Platform - IoT Core info in file [include/Setup.h](/include/Setup.h).

```cpp
// Fill in your WiFi networks SSID and password
#define SECRET_SSID         ""
#define SECRET_PASS         ""

// Fill in your Google Cloud Platform - IoT Core info
#define SECRET_PROJECT_ID   ""
#define SECRET_CLOUD_REGION ""
#define SECRET_REGISTRY_ID  ""
#define SECRET_DEVICE_ID    ""
```

- Build the project. PlatformIO: Build.
- Connect the board ES32-EVB via USB and upload the binary. PlatformIO: Upload.
- Open a terminal with the baud rate to 115200 and you will see the public key of the device.

```console
My public key in slot 0 is :
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERxw1L3LGRqjhDq3FJpIOVULxBRboLJmkPsVqPP/3
pis8YmHA69qwRNGHJIc7KCKNESAvbTlc0h8D4PxXi60IOQ==
-----END PUBLIC KEY-----
```

**NB: It is preferable to test the board connections before uploading the project.** Please use PlatformIO: Test to upload the special binaries to test the relays and the connection between the board and the chip ATEC608A.

## Configuring and Adding the Board to GCP IoT Core

<!-- TODO: Add images and more information about this. Like a tutorial -->
1. [Create a device registry](https://cloud.google.com/iot/docs/quickstart#create_a_device_registry)
2. [Add a device to the registry](https://cloud.google.com/iot/docs/quickstart#add_a_device_to_the_registry). **NB: "ES256" must be selected as the "Public key format". Paste the PEM public key generated on the board earlier into the "Public key value" text area.**

---

## Useful information

### Arduino Libraries Used

- [Google Cloud IoT Core JWT@1.1.10](https://github.com/GoogleCloudPlatform/google-cloud-iot-arduino) (Modified for this project)
- [Arduino_JSON@0.1.0](https://github.com/arduino-libraries/Arduino_JSON)
- [MQTT@2.4.7](https://github.com/256dpi/arduino-mqtt)
- [ArduinoECCX08@1.3.3](https://github.com/arduino-libraries/ArduinoECCX08) (Modified)
  - NOTE: [Error by compiling this library with platformIO](https://github.com/arduino-libraries/ArduinoECCX08/issues/3). Please edit ECCX08.cpp file as follows.

```cpp
/*************** Original File */
while (_wire->requestFrom((uint8_t)_address, (size_t)responseSize, (bool)true) != responseSize && retries--);
```

```cpp
/*************** Edited File */
#ifdef ARDUINO_ESP32_EVB
  while (_wire->requestFrom(static_cast<int>(_address), static_cast<int>(responseSize), static_cast<int>(true)) != responseSize && retries--);
#else
  while (_wire->requestFrom((uint8_t)_address, (size_t)responseSize, (bool)true) != responseSize && retries--);
#endif // ARDUINO_ESP32_EVB
```

### ESP32 EVB Pin Map

Source from : [ESP32 EVB Pin Map - Barth Development](https://www.barth-dev.de/misc/eqipment/esp32-evb-pin-map/)

|  Pin   | Name                    | HW_CON                             |  EXT  | UEXT  |  Pull   |
| :----: | ----------------------- | ---------------------------------- | :---: | :---: | :-----: |
| GPIO0  | GPIO0/XTAL1/CLKIN       | ETH Phy/extl oscil/UART-Bridge RTS |   1   |       |   UP    |
| GPIO1  | GPIO1/U0TXD             | UART_Bridge                        |   2   |       |         |
| GPIO2  | GPIO2/HS2_DATA0         | MicroSD/D0/UART Bridge             |   3   |   8   |   DN    |
| GPIO3  | GPIO3/U0RXD             | UART_Bridge                        |   4   |       |         |
| GPIO4  | GPIO4/U1TXD             |                                    |   5   |   3   |   DN    |
| GPIO5  | GPIO5/CAN-TX            | CAN PHY                            |   6   |       |   UP    |
| GPIO6  | GPIO6/SD_CLK            |                                    |   7   |       |         |
| GPIO7  | GPIO7/SD_DATA0          |                                    |   8   |       |         |
| GPIO8  | GPIO8/SD_DATA1          |                                    |   9   |       |         |
| GPIO9  | GPIO9/SD_DATA2          |                                    |  10   |       |         |
| GPIO10 | GPIO10/SD_DATA3         |                                    |  11   |       |         |
| GPIO11 | GPIO11/SD_CMD           |                                    |  12   |       |         |
| GPIO12 | GPIO12/IR_Transmit      | IR LED                             |  13   |       |   DN    |
| GPIO13 | GPIO13/I2C-SDA          |                                    |  14   |   6   | UP 2.2k |
| GPIO14 | GPIO14/HS2_CLK          | SD-CLK                             |  15   |   9   |         |
| GPIO15 | GPIO15/HS2_CM           | SD_CMD                             |  16   |   7   |   UP    |
| GPIO16 | GPIO16/I2C-SC           |                                    |  17   |   5   |         |
| GPIO17 | GPIO17/SPI_CS           |                                    |  18   |  10   | UP 10k  |
| GPIO18 | GPIO18/MDIO(RMII)       | ETH PHY                            |  19   |       |         |
| GPIO19 | GPIO19/EMAC_TXD0(RMII)  | ETH PHY                            |  20   |       |         |
| GPIO21 | GPIO21/EMAC_TX_EN(RMII) | ETH PHY                            |  21   |       |         |
| GPIO22 | GPIO22/EMAC_TXD1(RMII   | ETH PHY                            |  22   |       |         |
| GPIO23 | GPIO23/MDC(RMII)        | ETH PHY                            |  23   |       |         |
| GPIO25 | GPIO25/EMAC_RXD0(RMII)  | ETH PHY                            |  24   |       |         |
| GPIO26 | GPIO26/EMAC_RXD1(RMII)  | ETH PHY                            |  25   |       |         |
| GPIO27 | GPIO27/EMAC_RX_CRS_DV   | ETH PHY                            |  26   |       |         |
| GPIO32 | GPIO32/REL1             | Relay 1                            |  27   |       |         |
| GPIO33 | GPIO33/REL2             | Relay 2                            |  28   |       |         |
| GPI34  | GPI34/BUT1              | Button 1                           |  29   |       |         |
| GPI35  | GPI35/CAN-RX            | CAN PHY                            |  30   |       |         |
| GPI36  | GPI36/U1RXD             | (1N5819@UEXT)                      |  31   |   4   | UP 10k  |
| GPI39  | GPI39/IR_RECEIVE        | IR Receiver                        |  32   |       |         |