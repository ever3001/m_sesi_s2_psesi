
![Page en construction](img/page-en-construction.png)



```mermaid

graph TD
START((START)) --> P1(Initialiser le port serial)
P1 --> P2{Est-il initialisé?}
P2 -- oui --> P3(Initialiser communication I2C)
P2 -- non --> FIN
P3 --> P4{Est-il initialisé?}
P4 -- oui --> P5(Initialiser le crypto processus ATECC608A)
P4 -- non --> FIN
P5 --> P6{Est-il initialisé?}
P6 -- oui --> P7(Initialiser la communication <br/>WiFi et la date)
P6 -- non --> FIN
P7 --> P8{Est-il initialisé?}
P8 -- oui --> P9(Initialiser le client MQTT)
P8 -- non --> P7
P9 --> P10{Est-il initialisé?}
P10 -- oui --> P11(Se connecter au serveur de Google)
P10 -- non --> P9
P11 --> P12{Cinq secondes se<br/>sont-elles écoulées ?}
P12 -- oui --> P13(Envoyer l'information RSSI <br/>du WiFi au serveur)
P13 --> P14
P12 -- non --> P14{Est-ce qu'une <br/>commande <br/>est arrivée?}
P14 -- oui --> P15(Traiter la commande)
P15 --> P16
P14 -- non --> P16{Est-ce que la connexion <br/>avec le serveur est établie?}
P16 -- oui --> P12
P16 -- non --> P7
FIN((FIN))
```