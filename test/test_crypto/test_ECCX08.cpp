#include <Arduino.h>
#include <Arduino_JSON.h>
#include <iostream>
#include <unity.h>

#include <ECCX08.h>
#include <ESP_32_GPIO.h>
#include <Wire.h>
#include <utility/ECCX08DefaultTLSConfig.h>
#include <utility/ECCX08JWS.h>

String publicKeyTest;

/******************************* TESTS */

void
test_init_I2C(void)
{
  TEST_ASSERT(Wire.begin());
}

void
test_init_ECCX08(void)
{
  TEST_ASSERT(ECCX08.begin());
}

void
test_locked_ECCX08(void)
{
  // Verify if the chip is locked
  if (!ECCX08.locked()) {
    // if not configure and lock
    TEST_ASSERT(ECCX08.writeConfiguration(ECCX08_DEFAULT_TLS_CONFIG) == 0);
    TEST_ASSERT(ECCX08.lock());
  } else {
    TEST_ASSERT(ECCX08.locked());
  }
}

void
test_create_private_public_key_slot_0(void)
{
  // Create public key in slot 0
  publicKeyTest = ECCX08JWS.publicKey(0, false);

  // If public key is NULL or empty
  if (!publicKeyTest || publicKeyTest == "") {
    // Creates a new private/public key in slot 0
    publicKeyTest = ECCX08JWS.publicKey(0, true);
  }
  TEST_MESSAGE("The key is : ");
  TEST_MESSAGE(publicKeyTest.c_str());
  TEST_ASSERT(publicKeyTest && publicKeyTest != "");
}

void
test_JWT(void)
{
  // JSON for the header. ALGORITHM & TOKEN TYPE
  JSONVar jwtHeader;
  // JSON for the payload. DATA
  JSONVar jwtPayload;

  // Asymmetric Key Cryptography algorithm Elliptic Curve Digital Signature
  // Algorithm using P-256 and SHA-256.
  jwtHeader["alg"] = "ES256";
  jwtHeader["typ"] = "JWT";

  jwtPayload["test"] = "Hello world";
  String JwtBuff =
    ECCX08JWS.sign(0, JSON.stringify(jwtHeader), JSON.stringify(jwtPayload));
  TEST_MESSAGE("JWT to verify in https://jwt.io/ ");
  TEST_MESSAGE(JwtBuff.c_str());
  TEST_MESSAGE("Public Key");
  TEST_MESSAGE(publicKeyTest.c_str());
  //TODO: Find the way to assert this without jwt.io
}

/** default function */
void
setup()
{
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);

  UNITY_BEGIN(); // IMPORTANT LINE!
  RUN_TEST(test_init_I2C);
  RUN_TEST(test_init_ECCX08);
  RUN_TEST(test_locked_ECCX08);
  RUN_TEST(test_create_private_public_key_slot_0);
  RUN_TEST(test_JWT);
}

/** default function */
void
loop()
{
  UNITY_END(); // stop unit testing
}