#!/bin/sh
openssl genpkey -algorithm RSA -out rsa_private.pem -pkeyopt rsa_keygen_bits:2048
openssl rsa -in rsa_private.pem -pubout -out rsa_public.pem
openssl pkcs8 -topk8 -inform PEM -outform DER -in rsa_private.pem \
    -nocrypt > rsa_private_pkcs8