#!/bin/sh
openssl ecparam -genkey -name prime256v1 -noout -out ec_private.pem
openssl ec -in ec_private.pem -pubout -out ec_public.pem
openssl pkcs8 -topk8 -inform PEM -outform DER -in ec_private.pem \
    -nocrypt > ec_private_pkcs8