#include <Arduino.h>
#include <ESP_32_GPIO.h>
#include <unity.h>

void
test_led_builtin_pin_number(void)
{
  TEST_ASSERT_EQUAL(32, ESP_32_EVB_RELAY_1_GPIO);
  TEST_ASSERT_EQUAL(33, ESP_32_EVB_RELAY_2_GPIO);
}

void
test_led_state_high(void)
{
  digitalWrite(ESP_32_EVB_RELAY_1_GPIO, HIGH);
  TEST_ASSERT_EQUAL(HIGH, digitalRead(ESP_32_EVB_RELAY_1_GPIO));
  digitalWrite(ESP_32_EVB_RELAY_2_GPIO, HIGH);
  TEST_ASSERT_EQUAL(HIGH, digitalRead(ESP_32_EVB_RELAY_2_GPIO));
}

void
test_led_state_low(void)
{
  digitalWrite(ESP_32_EVB_RELAY_1_GPIO, LOW);
  TEST_ASSERT_EQUAL(LOW, digitalRead(ESP_32_EVB_RELAY_1_GPIO));
  digitalWrite(ESP_32_EVB_RELAY_2_GPIO, LOW);
  TEST_ASSERT_EQUAL(LOW, digitalRead(ESP_32_EVB_RELAY_2_GPIO));
}

void
setup()
{
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);

  UNITY_BEGIN(); // IMPORTANT LINE!
  RUN_TEST(test_led_builtin_pin_number);

  pinMode(ESP_32_EVB_RELAY_1_GPIO, OUTPUT);
  pinMode(ESP_32_EVB_RELAY_2_GPIO, OUTPUT);
}

uint8_t i = 0;
uint8_t max_blinks = 5;

void
loop()
{
  if (i < max_blinks) {
    RUN_TEST(test_led_state_high);
    delay(500);
    RUN_TEST(test_led_state_low);
    delay(500);
    i++;
  } else if (i == max_blinks) {
    UNITY_END(); // stop unit testing
  }
}