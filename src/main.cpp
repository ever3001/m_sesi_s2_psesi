/**
 * @file main.cpp
 * @author Ever ATILANO (ever.atilano_rosales@etu.upmc.fr)
 * @brief
 *
 */
#include "ESP32_IoT_Google_JWT_ECCX08.h"
#include "ESP_32_GPIO.h"
#include "Setup.h"
#include <Arduino.h>

#define TIME_TO_SEND_MESSAGE_IN_SEC (5)

unsigned long lastMillis = 0;

using namespace ESP32_IoT_Core;

// The MQTT callback function for commands and configuration updates
// Place your message handler code here.
void
messageReceived(String& topic, String& payload)
{
  String commandString = String("/devices/");
  commandString.concat(SECRET_DEVICE_ID);
  commandString.concat("/commands");
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic.equals(commandString)) {
    if (payload.equals("R1ON")) {
      digitalWrite(ESP_32_EVB_RELAY_1_GPIO, HIGH);
    } else if (payload.equals("R1OFF")) {
      digitalWrite(ESP_32_EVB_RELAY_1_GPIO, LOW);
    } else if (payload.equals("R2ON")) {
      digitalWrite(ESP_32_EVB_RELAY_2_GPIO, HIGH);
    } else if (payload.equals("R2OFF")) {
      digitalWrite(ESP_32_EVB_RELAY_2_GPIO, LOW);
    }
  }
}
///////////////////////////////

/** Setup of the program */
void
setup()
{
  // Init Serial Port
  Serial.begin(DEFAULT_SERIAL_BAUD_RATE);
  initESP32IoTCore();
  pinMode(ESP_32_EVB_RELAY_1_GPIO, OUTPUT);
  pinMode(ESP_32_EVB_RELAY_2_GPIO, OUTPUT);
}

void
loop()
{
  getCoreMQTT()->loop();
  delay(10); // <- fixes some issues with WiFi stability

  if (!getMQTTClient()->connected()) {
    ESP32_MQTT::connect();
  }

  // publish a message roughly every 5 seconds.
  if (millis() - lastMillis > (60000 * TIME_TO_SEND_MESSAGE_IN_SEC)) {
    lastMillis = millis();
    ESP32_MQTT::publishTelemetry(ESP32_WiFi::getRSSIWiFi());
  }
}