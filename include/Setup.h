#ifndef _SETUP_H_
#define _SETUP_H_

#include "ESP_32_GPIO.h"

/* Default baud rate for the serial monitor */
#define DEFAULT_SERIAL_BAUD_RATE (115200)



// Fill in your WiFi networks SSID and password
#define SECRET_SSID         ""
#define SECRET_PASS         ""

// Fill in your Google Cloud Platform - IoT Core info
#define SECRET_PROJECT_ID   ""
#define SECRET_CLOUD_REGION ""
#define SECRET_REGISTRY_ID  ""
#define SECRET_DEVICE_ID    ""

// Configuration for NTP
#define NTP_PRIMARY         "pool.ntp.org"
#define NTP_SECONDARY       "time.nist.gov"
/* UTC offset for your timezone: https://en.wikipedia.org/wiki/List_of_UTC_time_offsets */
#define UTC_GMT             (1)
#define GMT_OFFSET_SEC      (UTC_GMT * 3600 )
/* if your country observes Daylight saving time set it to 1 */
#define DAYLIGHT_OFFSET     (1)
#define DAYLIGHT_OFFSET_SEC (DAYLIGHT_OFFSET * 3600 )


// To get the private key run (where private-key.pem is the ec private key
// used to create the certificate uploaded to google cloud iot):
// openssl ec -in <private-key.pem> -noout -text
// and copy priv: part.
// The key length should be exactly the same as the key length bellow (32 pairs
// of hex digits). If it's bigger and it starts with "00:" delete the "00:". If
// it's smaller add "00:" to the start. If it's too big or too small something
// is probably wrong with your key.
#define PRIVATE_KEY_STR \
    "81:db:f4:bc:51:20:7c:96:20:96:ad:a5:cd:d0:83:" \
    "f2:b6:ff:e7:45:51:80:9c:81:f3:a2:61:5d:8f:21:" \
    "c6:f8"

// To get the certificate for your region run:
//   openssl s_client -showcerts -connect mqtt.googleapis.com:8883
// for standard mqtt or for LTS:
//   openssl s_client -showcerts -connect mqtt.2030.ltsapis.goog:8883
// Copy the certificate (all lines between and including ---BEGIN CERTIFICATE---
// and --END CERTIFICATE--) to root.cert and put here on the root_cert variable.
#define ROOT_CERT \
    "-----BEGIN CERTIFICATE-----\n" \
    "MIIErjCCA5agAwIBAgIQW+5sTJWKajts11BgHkBRwjANBgkqhkiG9w0BAQsFADBU\n" \
    "MQswCQYDVQQGEwJVUzEeMBwGA1UEChMVR29vZ2xlIFRydXN0IFNlcnZpY2VzMSUw\n" \
    "IwYDVQQDExxHb29nbGUgSW50ZXJuZXQgQXV0aG9yaXR5IEczMB4XDTE5MDYxMTEy\n" \
    "MzE1OVoXDTE5MDkwMzEyMjAwMFowbTELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNh\n" \
    "bGlmb3JuaWExFjAUBgNVBAcMDU1vdW50YWluIFZpZXcxEzARBgNVBAoMCkdvb2ds\n" \
    "ZSBMTEMxHDAaBgNVBAMME21xdHQuZ29vZ2xlYXBpcy5jb20wggEiMA0GCSqGSIb3\n" \
    "DQEBAQUAA4IBDwAwggEKAoIBAQDHuQUoDZWl2155WvaQ9AmhTRNC+mHassokdQK7\n" \
    "NxkZVZfrS8EhRkZop6SJGHdvozBP3Ko3g1MgGIZFzqb5fRohkRKB6mteHHi/W7Uo\n" \
    "7d8+wuTTz3llUZ2gHF/hrXFJfztwnaZub/KB+fXwSqWgMyo1EBme4ULV0rQZGFu6\n" \
    "7U38HK+mFRbeJkh1SDOureI2dxkC4ACGiqWfX/vSyzpZkWGRuxK2F5cnBHqRbcKs\n" \
    "OfmYyUuxZjGah+fC5ePgDbAntLUuYNppkdgT8yt/13ae/V7+rRhKOZC4q76HBEaQ\n" \
    "4Wn5UC+ShVaAGuo7BtfoIFSyZi8/DU2eTQcHWewIXU6V5InhAgMBAAGjggFhMIIB\n" \
    "XTATBgNVHSUEDDAKBggrBgEFBQcDATA4BgNVHREEMTAvghNtcXR0Lmdvb2dsZWFw\n" \
    "aXMuY29tghhtcXR0LW10bHMuZ29vZ2xlYXBpcy5jb20waAYIKwYBBQUHAQEEXDBa\n" \
    "MC0GCCsGAQUFBzAChiFodHRwOi8vcGtpLmdvb2cvZ3NyMi9HVFNHSUFHMy5jcnQw\n" \
    "KQYIKwYBBQUHMAGGHWh0dHA6Ly9vY3NwLnBraS5nb29nL0dUU0dJQUczMB0GA1Ud\n" \
    "DgQWBBSKWpFfG/yH1dkkJT05y/ZnRm/M4DAMBgNVHRMBAf8EAjAAMB8GA1UdIwQY\n" \
    "MBaAFHfCuFCaZ3Z2sS3ChtCDoH6mfrpLMCEGA1UdIAQaMBgwDAYKKwYBBAHWeQIF\n" \
    "AzAIBgZngQwBAgIwMQYDVR0fBCowKDAmoCSgIoYgaHR0cDovL2NybC5wa2kuZ29v\n" \
    "Zy9HVFNHSUFHMy5jcmwwDQYJKoZIhvcNAQELBQADggEBAKMoXHxmLI1oKnraV0tL\n" \
    "NzznlVnle4ljS/pqNI8LUM4/5QqD3qGqnI4fBxX1l+WByCitbTiNvL2KRNi9xau5\n" \
    "oqvsuSVkjRQxky2eesjkdrp+rrxTwFhQ6NAbUeZgUV0zfm5XZE76kInbcukwXxAx\n" \
    "lneyQy2git0voUWTK4mipfCU946rcK3+ArcanV7EDSXbRxfjBSRBD6K+XGUhIPHW\n" \
    "brk0v1wzED1RFEHTdzLAecU50Xwic6IniM3B9URfSOmjlBRebg2sEVQavMHbzURg\n" \
    "94aDC+EkNlHh3pOmQ/V89MBiF1xDHbZZ1gB0GszYKPHec9omSwQ5HbIDV3uf3/DQ\n" \
    "his=\n" \
    "-----END CERTIFICATE-----\n"

// Time (seconds) to expire token += 20 minutes for drift
#define GOOGLE_JWT_EXP_SECS (3600) // Maximum 24H (3600*24)


/****************** ATEC608A */
#define ATEC608_DEFAULT_SLOT (0) // (0 - 4)


/****************** ESP_32 */
/* Relays */
#define ESP_32_EVB_RELAY_1_INIT() pinMode(ESP_32_EVB_RELAY_1_GPIO, OUTPUT)
#define ESP_32_EVB_RELAY_2_INIT() pinMode(ESP_32_EVB_RELAY_2_GPIO, OUTPUT)

#define ESP_32_EVB_RELAY_1_HIGH() digitalWrite(ESP_32_EVB_RELAY_1_GPIO, HIGH)
#define ESP_32_EVB_RELAY_1_LOW() digitalWrite(ESP_32_EVB_RELAY_1_GPIO, LOW)
#define ESP_32_EVB_RELAY_2_HIGH() digitalWrite(ESP_32_EVB_RELAY_2_GPIO, HIGH)
#define ESP_32_EVB_RELAY_2_LOW() digitalWrite(ESP_32_EVB_RELAY_2_GPIO, LOW)

/* Buttons */
#define ESP_32_EVB_BTN_1_INIT() pinMode(ESP_32_EVB_BTN_1_GPIO, INPUT)

#define ESP_32_EVB_BTN_1_READ() digitalRead(ESP_32_EVB_BTN_1_GPIO)

/* Infrared Comunication */
#define ESP_32_EVB_IR_TRANSMIT_INIT()                                          \
  pinMode(ESP_32_EVB_IR_TRANSMIT_GPIO, OUTPUT)
#define ESP_32_EVB_IR_RECEIVE_INIT() pinMode(ESP_32_EVB_IR_RECEIVE_GPIO, INPUT)

#define ESP_32_EVB_IR_TRANSMIT_HIGH()                                          \
  digitalWrite(ESP_32_EVB_IR_TRANSMIT_GPIO, HIGH)
#define ESP_32_EVB_IR_TRANSMIT_LOW()                                           \
  digitalWrite(ESP_32_EVB_IR_TRANSMIT_GPIO, LOW)
#define ESP_32_EVB_IR_RECEIVE_READ() digitalRead(ESP_32_EVB_IR_RECEIVE_GPIO)

#endif // !_SETUP_H_