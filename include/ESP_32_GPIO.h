#ifndef _ESP_32_EVB_H_
#define _ESP_32_EVB_H_

/* Relays */
#define ESP_32_EVB_RELAY_1_GPIO       (32)
#define ESP_32_EVB_RELAY_2_GPIO       (33)

/* Buttons */
#define ESP_32_EVB_BTN_1_GPIO         (34)

/* I2C */
#define ESP_32_EVB_I2C_SDA_GPIO       (13)
#define ESP_32_EVB_I2C_SCL_GPIO       (16)

/* Infrared Comunication */
#define ESP_32_EVB_IR_TRANSMIT_GPIO   (12)
#define ESP_32_EVB_IR_RECEIVE_GPIO    (39)

#endif // !_ESP_32_EVB_H_